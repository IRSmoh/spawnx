#SpawnX#
A component/entity system
###Rationale:###
* Component data should be stored in contiguous memory, to prevent memory fragmentation/cache misses.
* Handles should be usable similar to pointers, with sane defaults and otherwise handle identical.
* Minimal overhead for dereferencing a handle (basically 2 pointer chases)
* Systems should be trivial to write, leaving the system::update function doing the bulk of the actual logic.
* Entities should be a glorified header for handles and be able to store any handle.
* Events should be trivial to write, and be auto fed any events that a system triggers (basically a delegate)

###Dependencies:###
C++14 (Tested against Visual Studio 2015 and Clang v 3.7)

cpp_delegate (included in the project already) [https://bitbucket.org/IRSmoh/cpp_delegate](https://bitbucket.org/IRSmoh/cpp_delegate) 

###Example:###
Basic hello world:

```
#!c++
#include <iostream>
#include "spawnx.h"
#include <string>
struct hello_world
{
	std::string str;
	hello_world()
	{
		str = "hello world";
	}
	void print()
	{
		std::cout << str << std::endl;
	}
};
//create a system that inherits from spawnx::system, templating system<> for the types we want to accept.
//supports...
//all<type> to get a std::vector<handle<type>>.
//none<type> to filter out entities that have the type (does not add to pack size.)
//all other types are assumed to be a handle<type> and will resolve as such.
struct hello_system : spawnx::system<hello_world>
{
	void update(double delta_time) override
	{
		//all of our handles are stored in a std::vector<handle_pack<components...>>
		//where handle_pack<> is a glorified wrapper for std::tuple<>
		//to access a specific component specify the component, and you'll get a spawnx::handle<>& returned.
		for (auto& pack : handle_packs)
		{
			pack.get<hello_world>()->print();
		}
	}
};
int main()
{
	//create our world
	//it stores all our entities, and any systems.
	spawnx::world world;

	//register our handle type.
	spawnx::component_ID::init_value<hello_world>();
	
	//create an entity
	//we use a weak_handle here so we can manipulate the entity however we want.
	//if the entity were to be removed from the world, we need an indicator of that (the entity should be dead).
	spawnx::weak_handle<spawnx::entity> e = world.create_entity();
	
	//lock our handle; similar to weak_ptr and shared_ptr.
	if (spawnx::handle<spawnx::entity> entity = e.lock())
	{
		//add our component(s)
		entity->add_component<hello_world>();
	}

	//register a system.
	world.add_system<hello_system>();

	//update all systems / event listeners
	world.update(0);
}

```

Example of how to use events:


```
#!c++

#include <iostream>
#include "spawnx.h"
#include <string>
struct name
{
	std::string str;
	name(const std::string& _str) : str(_str) {}
	name(const char* _str) : str(_str) {}
	name(){}
	void Print()
	{
		std::cout << "entities name is: " << str << std::endl;
	}
};
struct Vector2
{
	float x, y;
	Vector2(float _x, float _y) : x(_x), y(_y) {}
	Vector2() {}
};
struct name_system : public spawnx::system<name>
{
	void update(double delta_time)
	{
		for (spawnx::handle_pack<name>& pack : handle_packs)
		{
			pack.get<name>()->Print();
		}
	}
};
//showing that we can use multiple component types in a single system.
struct height_check_system : public spawnx::system<name, Vector2>
{
	void update(double delta_time)
	{
		for (decltype(auto) pack : handle_packs)
		{
			//this is done to prove you can dereference the handle and keep a valid reference to your data
			//thus preventing calling pack.get<>() and the handle deref types all the time.
			//they are lite... but they still have to do a couple effective pointer chases.
			Vector2& vec = *pack.get<Vector2>();
			if (vec.y <= 0.f)
			{
				std::cout << "The entity by name: [" << pack.get<name>()->str << "] is below y = 0.\n";
			}
		}
	}
};
//a simple system that emits an event.
struct height_event_emitter_system : public spawnx::system<Vector2>
{
	void update(double delta_time)
	{
		for (decltype(auto) pack : handle_packs)
		{
			const Vector2& vec = *pack.get<Vector2>();
			if (vec.x < 0 && vec.y < 0)
			{
				//spawnx::system has a helper function called emit_event<> where we can feed it our data easily.
				//otherwise we'd have to remember the longer version.. event_manager->add_event(_event);
				emit_event(vec);
			}
		}
	}
};

//a free standing function that we bind as a delegate to parse given events.
//the event in this case being a simple vector2.
void process_vec2_event(const Vector2& vec)
{
	std::cout << "This vector2 is in quadrant 3. [x < 0 , y <0]\nAnd the vec is: [" << vec.x << "," << vec.y << "]\n";
}

int main()
{
	//create the world again.
	spawnx::world world;
	//initialize our component types, so we can have valid storage.
	spawnx::component_ID::init_value<name, Vector2>();
	//we also have to initialize the event type as events and components do not share storage (since they are different things).
	spawnx::event_ID::init_value<Vector2>();
	//create our entity again.
	spawnx::weak_handle<spawnx::entity> entity = world.create_entity();
	if (spawnx::handle<spawnx::entity> e = entity.lock())
	{
		//add multiple components in one go.
		e->add_component<name,Vector2>("asd",Vector2(-10,-1));
	}
	//register our systems, this version indicates we can have non default constructed systems
	//but for this example we just default construct anyway.
	world.add_system(name_system());
	//register multiple default constructed systems.
	world.add_system<height_check_system,height_event_emitter_system>();
	//register the previously done listener.
	world.add_event_listener<Vector2>().func<&process_vec2_event>();
	//update as always.
	world.update(0);
}
```

##License##

Distributed under the Boost Software License, Version 1.0 [ http://www.boost.org/LICENSE_1_0.txt]( http://www.boost.org/LICENSE_1_0.txt)

Go crazy.