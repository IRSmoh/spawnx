#pragma once
#include <tuple>
namespace zip
{
	template<typename itr_type, typename... rest>
	class iterator
	{
	protected:
		using itr_tuple = std::tuple<itr_type, rest...>;
		itr_tuple itrs;

	public:
		//a "magic" class that when supplied with indicies will allow us to unpack a tuple that contains iterators, derefence them, and rebuild a tuple from that.
		//this is so we can mimic a normal iterator, but apply it to multiple (e.g. a proper zip_iterator.)
		template<typename ... garbage>struct get_tuple_as {};
		template<size_t... indices>
		struct get_tuple_as<std::index_sequence<indices...>>
		{
			//apparently calling remove_references<decltype(*get<indices>(tuple))>::type... doesn't work well.
			template<typename ... types>
			using tuple_with_remove_references = std::tuple<typename std::remove_reference<types>::type...>;

			using itr_tuple = std::tuple<itr_type, rest...>;
			using ref_tuple = std::tuple<decltype(*std::get<indices>(itr_tuple()))&...>;
			using obj_tuple = tuple_with_remove_references<decltype(*std::get<indices>(itr_tuple()))...>;
			using ptr_tuple = std::tuple<decltype(&(*std::get<indices>(itr_tuple())))...>;

			static ref_tuple refs(const itr_tuple& tuple_itrs)
			{
				return ref_tuple(*std::get<indices>(tuple_itrs)...);
			}
			static obj_tuple objs(const itr_tuple& tuple_itrs)
			{
				return obj_tuple(*std::get<indices>(tuple_itrs)...);
			}
			static void increment(itr_tuple& tuple_itrs)
			{
				char op[] = { (++std::get<indices>(tuple_itrs), '\0')... };	//hack for allowing us to expand our tuple and call our operator on it.
				(void)op;	//supress unused variable for those compilers that would complain.
			}
			static void decrement(itr_tuple& tuple_itrs)
			{
				char op[] = { (--std::get<indices>(tuple_itrs), '\0')... };
				(void)op;
			}
			static itr_tuple increment_distance(itr_tuple& tuple_itrs, ptrdiff_t distance)
			{
				return itr_tuple((std::get<indices>(tuple_itrs) + distance)...);
			}
		};
		template<size_t ... indx>
		decltype(auto) refs(std::index_sequence<indx...>)
		{
			return std::tuple<decltype(*std::get<indx>(itrs))...>(*std::get<indx>(itrs)...);
		}
		using itr_tuple_unpacker = get_tuple_as<std::make_index_sequence<std::tuple_size<itr_tuple>::value>>;
		//typedefs to make our lives easier; and because iterator's require them in the stl/boost implementations.
		//but this base type of iterator is essentially never supplied in the first place to an algorithm so we don't typedef its category.

		using reference = typename itr_tuple_unpacker::ref_tuple;
		using value_type = typename itr_tuple_unpacker::obj_tuple;
		using pointer = typename itr_tuple_unpacker::ptr_tuple;
		using difference_type = ptrdiff_t;

		iterator() {}
		iterator(const std::tuple<itr_type, rest...>& itr_pack) : itrs(itr_pack) {}
		iterator(const itr_type& first_itr, const rest&... rest_itrs) :
			itrs(itr_tuple(first_itr, rest_itrs...)) {}
		iterator(itr_type&& first_itr, rest&&... rest_itrs) :
			itrs(itr_tuple(std::forward<itr_type>(first_itr), std::forward<rest>(rest_itrs)...)) {}

		void increment()
		{
			itr_tuple_unpacker::increment(itrs);
		}
		friend void swap(iterator& left, iterator& right)
		{
			std::swap(left, right);
		}
		iterator& operator++()
		{
			increment();
			return *this;
		}
		iterator operator++(int)
		{
			auto tmp = *this;
			increment();
			return tmp;
		}

		friend void inline iter_swap(iterator left, iterator right)
		{
			value_type tmp = *left;
			*left = *right;
			*right = tmp;
		}
		friend inline void swap(reference& left, reference& right)
		{
			value_type tmp = left;
			left = right;
			right = tmp;
		}
		friend void swap(reference left, reference right)
		{
			std::swap(left, right);
		}
	};
	//normally a forward iterator would inherit from output and input iterator.. but to do so we have to use virtual inheritance on the base iterator type.
	//this causes our iterator to be 1 virtual pointer larger than it techinically has to.
	//also, no stl algorithm/struct uses just output/input... so we can make them redudant and just roll it into forward_iterator.

	template<typename itr_type, typename... rest>
	class forward_iterator : public iterator<itr_type, rest...>
	{
		using base_type = iterator<itr_type, rest...>;
	public:
		using iterator_category = std::forward_iterator_tag;
		using iterator<itr_type, rest...>::iterator;

		typename base_type::reference operator*()
		{
			return base_type::itr_tuple_unpacker::refs(base_type::itrs);
		}
		bool operator==(const forward_iterator& right)
		{
			return base_type::itrs == right.itrs;
		}
		bool operator!=(const forward_iterator& right)
		{
			return !(*this == right);
		}
		forward_iterator& operator++()
		{
			base_type::increment();
			return *this;
		}
		forward_iterator operator++(int)
		{
			auto tmp = *this;
			base_type::increment();
			return tmp;
		}
	};
	template<typename itr_type, typename... rest>
	class bidirectional_iterator : public forward_iterator<itr_type, rest...>
	{
		using base_type = iterator<itr_type, rest...>;
	public:
		using iterator_category = std::bidirectional_iterator_tag;
		using forward_iterator<itr_type, rest...>::forward_iterator;

		void decrement()
		{
			base_type::itr_tuple_unpacker::decrement(base_type::itrs);
		}
		bidirectional_iterator& operator--()
		{
			decrement();
			return *this;
		}
		bidirectional_iterator operator--(int)
		{
			auto tmp = *this;
			decrement();
			return tmp;
		}
		bidirectional_iterator& operator++()
		{
			base_type::increment();
			return *this;
		}
		bidirectional_iterator operator++(int)
		{
			auto tmp = *this;
			base_type::increment();
			return tmp;
		}
	};

	template<typename itr_type, typename... rest>
	class random_access_iterator : public bidirectional_iterator<itr_type, rest...>
	{
		using base_type = iterator<itr_type, rest...>;
	public:
		using iterator_category = std::random_access_iterator_tag;
		using bidirectional_iterator<itr_type, rest...>::bidirectional_iterator;


		bool operator<(const random_access_iterator& right)
		{
			return base_type::itrs < right.itrs;
		}
		bool operator>(const random_access_iterator& right)
		{
			return base_type::itrs > right.itrs;
		}
		bool operator<=(const random_access_iterator& right)
		{
			return !(*this > right);
		}
		bool operator>=(const random_access_iterator& right)
		{
			return !(*this < right);
		}
		random_access_iterator& operator++()
		{
			base_type::increment();
			return *this;
		}
		random_access_iterator operator++(int)
		{
			auto tmp = *this;
			base_type::increment();
			return tmp;
		}
		random_access_iterator operator+(ptrdiff_t diff)
		{
			return base_type::itr_tuple_unpacker::increment_distance(base_type::itrs, diff);
		}
		random_access_iterator& operator+=(ptrdiff_t diff)
		{
			return *this = *this + diff;
		}
		friend random_access_iterator operator+(ptrdiff_t diff, const random_access_iterator& right)
		{
			return right + diff;
		}
		random_access_iterator operator-(ptrdiff_t diff)
		{
			return *this + (-diff);
		}
		random_access_iterator& operator-=(ptrdiff_t diff)
		{
			return *this = *this - diff;
		}
		typename base_type::difference_type operator-(const random_access_iterator& right)
		{
			//the references cannot drift appart from each other as they all increase/decrease at the same rates.
			return std::get<0>(base_type::itrs) - std::get<0>(right.itrs);
		}
		//we shouldn't need to specify the base class.. but apparently the compiler loses track of 'deeply' nested virtual inheritance using statements.
		typename base_type::reference operator [](size_t offset)
		{
			return base_type::itr_tuple_unpacker::refs(base_type::itr_tuple_unpacker::increment_distance(base_type::itrs, offset));
		}
	};

	template<typename ... types>
	random_access_iterator<types...> random(types&&... itrs)
	{
		return random_access_iterator<types...>(std::forward<types>(itrs)...);
	}
	template<typename ... types>
	bidirectional_iterator<types...> bidirectional(types&&... itrs)
	{
		return bidirectional_iterator<types...>(std::forward<types>(itrs)...);
	}
	template<typename...types>
	forward_iterator<types...> forward(types&&... itrs)
	{
		return forward_iterator<types...>(std::forward<types>(itrs)...);
	}
}
//yeah this is bad; oh well.
//we can't send a newly created temporary to a function that expects a reference on vs 2015 clang 64bit if clang is right; broken; or microsoft is wrong I don't know.
//but breaking the rule 'don't overload the std:: namespace' allows us to properly swap temporaries of tuple<types&...> (no ref) which is literally only used in iter_swap{ swap(*itr,*itr2); }

namespace std
{
	template<typename ... types>
	inline void swap(std::tuple<types&...>&& left, std::tuple<types&...>&& right)
	{
		left.swap(right);
	}
}