#pragma once
#include <cstddef>
#include <stdexcept>
#include <cstring>
#include <ostream>
#include <string>
namespace spawnx
{
	class static_string
	{
		const char* const data_;
		const std::size_t size_;
	public:
		typedef const char* const_iterator;

		template <std::size_t _size>
		constexpr static_string(const char(&cstr)[_size]) noexcept
			: data_(cstr)
			, size_(_size)
		{}

		constexpr static_string(const char* cstr_ptr, std::size_t _size) noexcept
			: data_(cstr_ptr)
			, size_(_size)
		{}
		//note:
		//the debug data for this will appear bogus; aka assuming the begin_itr and end_itr point into a sub range of the supplied str
		//data_ will show more of the string contents than this static_string 'owns'
		//but when you convert the static_string to a normal std::string the data will copy over properly.
		//e.g......
		//data_ = "1234567890"
		//size_ = 3
		//means we 'own' "123" but since debug info reads to the zero byte it'll appear we own the whole thing.
		//its kinda stuck this way until I can figure out how to copy const char* sub ranges in a constexpr way.

		constexpr static_string(const static_string& str, const_iterator begin_itr, const_iterator end_itr) noexcept
			: data_(str.data() + (begin_itr - str.data()))
			, size_(end_itr - begin_itr)
		{}

		constexpr const char* data() const noexcept { return data_; }
		constexpr std::size_t size() const noexcept { return size_; }

		constexpr const_iterator begin() const noexcept { return data_; }
		constexpr const_iterator end()   const noexcept { return data_ + size_; }

		constexpr char operator[](std::size_t ind) const
		{
			return ind < size_ ? data_[ind] : throw std::out_of_range("static_string");
		}
		constexpr bool operator==(const static_string& right) const
		{
			return data_ == right.data() && size_ == right.size();
		}
		constexpr bool operator!=(const static_string& right) const
		{
			return !(*this == right);
		}
		operator std::string() const
		{
			return std::string(begin(), end());
		}
		constexpr friend const_iterator find(const static_string& str, const char& c)
		{
			return find_impl(str, str.begin(), 1, c);
		}
		constexpr friend const_iterator rfind(const static_string& str, const char& c)
		{
			return find_impl(str, str.end() - 1, -1, c);
		}
	private:
		static constexpr const_iterator find_impl(const static_string& str, const_iterator curr_itr, int increment, const char& c)
		{
			// yeah.... this is a recursive terneray statement... I blame constexpr not being able to have for loops causing this to happen.
			return *curr_itr == c				//check if we've found our itr location 
				? curr_itr						//we have, return it back.
				: curr_itr == str.end()			//we have not found the character check if we've reached the end or not.
				? str.end()						//reached the end
				: find_impl(str, curr_itr + increment, increment, c);	//we are not at the end try and find the character somewhere else.
		}
	};
	inline std::ostream& operator<<(std::ostream& os, static_string const& str)
	{
		return os.write(str.data(), str.size());
	}
}