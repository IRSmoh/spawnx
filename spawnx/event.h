#pragma once
namespace spawnx
{
	class event_manager
	{
		friend class world;
		friend class base_system;
		template<typename... all>
		friend class system;
		class event_storage_base
		{
		public:
			event_storage_base() {}
			virtual ~event_storage_base() {}
			virtual void update() {}
		};
		template<typename... event_types>
		class event_storage : public event_storage_base
		{
			std::vector<std::tuple<event_types...>> events;
			delegate<void, const event_types&...> delegates;
		public:
			event_storage() {}
			~event_storage() {}
			void update() override
			{
				delegates(events);
				if (!events.empty())
					events.clear();
			}
			template<typename... types>
			void add_event(types&&... params)
			{
				events.push_back(std::make_tuple(std::forward<types>(params)...));
			}
			delegate<void, const event_types&...>& get_delegates()
			{
				return delegates;
			}
		};
		std::vector<std::unique_ptr<event_storage_base>> event_storages;
		event_manager() {}

		//we have to specify this stuff below because somehow visual studio currently can't understand that a vector of unique_ptrs is move only.
		event_manager(const event_manager&) = delete;
		event_manager(event_manager&&) = default;
		event_manager& operator=(const event_manager&) = delete;
		event_manager& operator=(event_manager&&) = default;
		~event_manager() {}

		template<typename ... event_types>
		::spawnx::errors::error_code add_event_type()
		{
			unsigned int index = event_id->get_grouped_value<event_types...>();

			if (index == event_ID::TYPE_NOT_INIT)
				return ::spawnx::errors::uninit_type;			//can't use this ID yet due to the ID not being init.

			if (index < event_storages.size())
			{
				if (event_storages[index])
					return ::spawnx::errors::duplicate_alloc;	//we've allocated storage for something in this slot
				event_storages[index] = std::unique_ptr<event_storage<event_types...>>(new event_storage<event_types...>());
				return ::spawnx::errors::success;
			}
			if (index > event_storages.size())
			{
				//we've got a request to make a 'high' index store.
				//aka we've somehow skipped the normal sequential pattern.
				event_storages.resize(index);
			}
			event_storages.push_back(std::unique_ptr<event_storage<event_types...>>(new event_storage<event_types...>()));
			return errors::success;
		}
	public:
		template<typename ... event_types>
		void add_event(event_types&&... params)
		{
			unsigned int index = event_id->get_grouped_value<std::decay_t<event_types>...>();
			auto tmp = std::make_tuple(type_name<event_types>()...);
			if (index == event_ID::TYPE_NOT_INIT)
			{
				assert(!("Trying to create an event with an unitialized event type."));
				return;
			}
			if (index >= event_storages.size() || event_storages[index] == nullptr)
			{
				add_event_type<std::decay_t<event_types>...>();
			}
			event_storage<std::decay_t<event_types>...>* events = static_cast<event_storage<std::decay_t<event_types>...>*>(event_storages[index].get());
			events->add_event(std::forward<event_types>(params)...);
		}
	private:
		void update()
		{
			for (auto& events_stored : event_storages)
			{
				if (events_stored)
					events_stored->update();
			}
		}

		//we are now only passed event types from func_arg_solver_t and the packs given to us are all std::tuples.
		//empty is actually ironically faster to use than any other method of breaking about a tuple's types.
		//it takes up 1 byte on the stack vs any reference taking up minimum sizeof(ptr) (4-8 bytes) or worse depending on how many additional args have to be passed to a static function.

		template<typename... event_types>
		event_storage<std::remove_cv_t<std::remove_reference_t<event_types>>...>* get_event_storage(empty<std::tuple<event_types...>>)
		{
			unsigned int index = event_id->get_grouped_value<std::remove_cv_t<std::remove_reference_t<event_types>>...>();

			if (index == event_ID::TYPE_NOT_INIT)
			{
				assert(!("Trying to create an event with an unitialized event type."));
				return nullptr;
			}
			if (index >= event_storages.size())
			{
				add_event_type<std::remove_cv_t<std::remove_reference_t<event_types>>...>();
			}
			return static_cast<event_storage<std::remove_cv_t<std::remove_reference_t<event_types>>...>*>(event_storages[index].get());
		}
		template<typename func, std::enable_if_t<std::is_pointer<func>::value, bool> = true>
		void add(func function)
		{
			get_event_storage(empty<typename func_arg_solver_t<func>::args>{})->get_delegates().add(function);
		}
		template<typename lambda, std::enable_if_t<!std::is_pointer<lambda>::value, bool> = false>
		void add(lambda lam)
		{
			//if this throws a compiler error for you (because you're using a varidiac lambda.. aka of the form [](auto x){}) specify the template type; we can't auto deduce that.
			get_event_storage(empty<typename func_arg_solver_t<decltype(&lambda::operator())>::args>{})->get_delegates().add(lam);
		}
		template<typename first_lam_arg, typename ... lam_rest_args, typename lambda, std::enable_if_t<!std::is_pointer<lambda>::value, bool> = false>
		void add(lambda lam)
		{
			get_event_storage(empty<std::tuple<first_lam_arg, lam_rest_args...>>{})->get_delegates().add(lam);
		}
		template<typename obj, typename method>
		void add(obj* object, method func)
		{
			get_event_storage(empty<typename func_arg_solver_t<method>::args>{})->get_delegates().add(object, func);
		}
		//literally all the same as above just with the library user specifying when the objects destruct vs auto managed/permanent.

		template<typename func, std::enable_if_t<std::is_pointer<func>::value, bool> = true>
		del_destruct_flag add_self_managed(func function)
		{
			return get_event_storage(empty<typename func_arg_solver_t<func>::args>{})->get_delegates().add_self_managed(function);
		}
		template<typename lambda, std::enable_if_t<!std::is_pointer<lambda>::value, bool> = false>
		del_destruct_flag add_self_managed(lambda lam)
		{
			//if this throws a compiler error for you (because you're using a varidiac lambda.. aka of the form [](auto x){}) specify the template type; we can't auto deduce that.
			return get_event_storage(empty<typename func_arg_solver_t<decltype(&lambda::operator())>::args>{})->get_delegates().add_self_managed(lam);
			//since the lambda will (currently) always have the const ref format, and our selecter expects
		}
		template<typename first_lam_arg, typename... lam_rest_args, typename lambda, std::enable_if_t<!std::is_pointer<lambda>::value, bool> = false>
		del_destruct_flag add_self_managed(lambda lam)
		{
			return get_event_storage(empty<std::tuple<first_lam_arg, lam_rest_args...>>{})->get_delegates().add_self_managed(lam);
		}
		template<typename obj, typename method>
		del_destruct_flag add_self_managed(obj* object, method func)
		{
			return get_event_storage(empty<typename func_arg_solver_t<method>::args>{})->get_delegates().add_self_managed(object, func);
		}
	};
}