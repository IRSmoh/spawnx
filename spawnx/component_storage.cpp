#include "spawnx.h"
namespace spawnx
{
	const component_vec_base::handle_offset component_vec_base::EMPTY_SLOT = handle_offset(-1);
	const component_index component_vec_base::INVALID_INDEX = component_index(-1, -1);
}