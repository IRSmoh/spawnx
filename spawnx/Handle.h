#pragma once
namespace spawnx
{
	class handle_base
	{
		friend class world;
		friend class entity;

		template<typename component>
		friend class weak_handle;
		template<typename component>
		friend class handle;
	protected:
		component_index index;
		unsigned int comp_id;

		static component_storage* storage;	//no reason to have this as a pointer, and no reason to have multiple storages since its just a static anyway.
		const static unsigned int INVALID_ID= -1;
		const static component_index INVALID_INDEX;
		//this constructor is meant to be used with ::clone() and for converting a weak_handle_base to a handle_base.; but we do not clone the data in the constructor thats handled in ::clone() itself.
		handle_base(component_index copied_index, unsigned int _component_id)
		{
			index = copied_index;
			comp_id = _component_id;
			component_vec_base* sub_storage = storage->get_storage_unsafe(comp_id);
			sub_storage->increment_active_component(copied_index);
		}
		handle_base()
		{
			index = INVALID_INDEX;
			comp_id = INVALID_ID;
		}
	public:
		handle_base(const handle_base& right)
		{
			comp_id = right.comp_id;
			index = right.index;
			if (!index.valid())
				return;
			component_vec_base* sub_storage = storage->get_storage_unsafe(comp_id);
			sub_storage->increment_active_component(index);
		}
		handle_base(handle_base&& right)
		{
			comp_id = right.comp_id;
			index = right.index;
			
			right.comp_id = INVALID_ID;
			right.index = INVALID_INDEX;
		}
		handle_base& operator=(const handle_base& right)
		{
			comp_id = right.comp_id;
			index = right.index;
			if (!index.valid())
				return *this;
			component_vec_base* sub_storage = storage->get_storage_unsafe(comp_id);
			sub_storage->increment_active_component(index);
			return *this;
		}
		handle_base& operator=(handle_base&& right)
		{
			comp_id = right.comp_id;
			index = right.index;

			right.comp_id = INVALID_ID;
			right.index = INVALID_INDEX;
			return *this;
		}
		~handle_base()
		{
			if (!index.valid())
				return;
			component_vec_base* sub_storage = storage->get_storage_unsafe(comp_id);
			sub_storage->decrement_active_component(index);
		}

		handle_base clone() const
		{
			if (!(index.valid() && comp_id != component_ID::TYPE_NOT_INIT))
				return handle_base();
			return handle_base(storage->get_storage_unsafe(comp_id)->clone_slot(index), comp_id);
		}
		std::string get_string()
		{
			if (!(index.valid() && comp_id != component_ID::TYPE_NOT_INIT))
				return "";
			return std::string("type_ID | comp_ID: {") + std::to_string(comp_id) + ',' + std::to_string(index.id) + '}' + std::string("\t")+ storage->get_storage_unsafe(comp_id)->get_string_of_component(index);
		}
		void set_owning_entity(unsigned int entity_id)
		{
			storage->get_storage_unsafe(comp_id)->set_handles_owning_entity(index, entity_id);
		}
		bool operator==(handle_base right) const
		{
			return (index == right.index) && (comp_id == right.comp_id);
		}
		bool operator!=(handle_base right) const
		{
			return !(*this == right);
		}
		bool is_valid() const
		{
			return index.valid() && comp_id != -1 && storage->get_storage_unsafe(comp_id)->component_unchanged(index);
		}
		explicit operator bool() const
		{
			return is_valid();
		}
		bool operator!() const
		{
			return !is_valid();
		}
	};

	//this needs to be declared before handle so we can exclude weak_handles from being a valid handle type.
	//it really; really makes no sense to have a handle<weak_handle<int>> when you're just going to return a weak_handle<int> that then must be locked.
	//its just a waste of space/pointer indirections.
	template<typename component>
	class weak_handle;

	template<typename component>
	class handle : public handle_base , disable_type<component, spawnx::handle_base>, disable_nesting<component, spawnx::handle, spawnx::weak_handle>
	{
		template<typename all>
		friend class weak_handle;
		//no extra members should be allowed here
		//that way we can ignore polymorphism and just force cast to the type we want
		//so that easy overload methods work properly
		//this constructor is meant to be used with ::clone(); but we do not clone the data in the constructor thats handled in ::clone() itself.
		handle(component_index copied_index)
		{
			index = copied_index;
			comp_id = component_id->get_value<component>();
			component_vec<component>* sub_storage = storage->get_storage<component>();
			sub_storage->increment_active_component(copied_index);
		}
	public:
		handle()
		{
			comp_id = component_id->get_value<component>();
			if (comp_id == component_ID::TYPE_NOT_INIT)
			{
				assert(!"Attempted to use an invalid Component_ID");
				index = INVALID_INDEX;
				return;
			}
			component_vec<component>* sub_storage = storage->get_storage<component>();
			index = sub_storage->assign_new_slot();

			sub_storage->increment_active_component(index);
		}
		handle(unsigned int entity_id) : handle()
		{
			if (comp_id == component_ID::TYPE_NOT_INIT)
			{
				assert(!"Attempted to use an invalid Component_ID");
				index = INVALID_INDEX;
				return;
			}
			storage->get_storage<component>()->set_handles_owning_entity(index, entity_id);
		}
		handle(component&& comp)
		{
			comp_id = component_id->get_value<component>();
			if (comp_id == component_ID::TYPE_NOT_INIT)
			{
				assert(!"Attempted to use an invalid Component_ID");
				index = INVALID_INDEX;
				return;
			}
			component_vec<component>* sub_storage = storage->get_storage<component>();
			index = sub_storage->assign_new_slot(std::forward<component>(comp));

			sub_storage->increment_active_component(index);
		}
		handle(component&& comp, unsigned int entity_id) : handle(std::forward<component>(comp))
		{
			if (comp_id == component_ID::TYPE_NOT_INIT)
			{
				assert(!"Attempted to use an invalid Component_ID");
				index = INVALID_INDEX;
				return;
			}
			storage->get_storage<component>()->set_handles_owning_entity(index, entity_id);
		}
		handle(const handle& right)
		{
			comp_id = right.comp_id;
			index = right.index;
			if (!index.valid())
				return;
			component_vec<component>* sub_storage = storage->get_storage<component>();
			sub_storage->increment_active_component(index);
		}
		handle(handle&& right)
		{
			//we do not have to set the owning entity when moving a handle; as we aren't actually reassigning the owner.
			//we're just straight stealing all the data from a different handle into this one.

			comp_id = right.comp_id;
			index = right.index;

			right.comp_id = INVALID_ID;
			right.index = INVALID_INDEX;
		}
		//if you're calling this you better know what your doing with your casts.
		handle(const handle_base& right)
		{
			comp_id = right.comp_id;
			index = right.index;
			if (!index.valid())
				return;
			component_vec<component>* sub_storage = storage->get_storage<component>();
			sub_storage->increment_active_component(index);
		}
		handle& operator=(const handle& right)
		{
			comp_id = right.comp_id;
			index = right.index;
			if (!index.valid())
				return *this;
			component_vec<component>* sub_storage = storage->get_storage<component>();
			sub_storage->increment_active_component(index);
			return *this;
		}
		handle& operator=(handle&& right)
		{
			comp_id = right.comp_id;
			index = right.index;

			right.comp_id = INVALID_ID;
			right.index = INVALID_INDEX;
			return *this;
		}

		component* operator->() const
		{
			assert(index.valid() && "Check to make sure you're not calling an unitialized component_ID/handle, or verify that your weak_ptr is actually valid.");
			component_vec<component>* sub_storage = storage->get_storage<component>();
			return &sub_storage->get_component(index);
		}
		component& operator*() const
		{
			assert(index.valid() && "Check to make sure you're not calling an unitialized component_ID/handle, or verify that your weak_ptr is actually valid.");
			component_vec<component>* sub_storage = storage->get_storage<component>();
			return sub_storage->get_component(index);
		}
		handle& operator=(const component& right)
		{
			assert(index.valid() && "Check to make sure you're not calling an unitialized component_ID/handle, or verify that your weak_ptr is actually valid.");
			if (index.valid())
			{
				component_vec<component>* sub_storage = storage->get_storage<component>();
				sub_storage->get_component(index) = right;
			}
			return *this;
		}
		handle& operator=(component&& right)
		{
			assert(index.valid() && "Check to make sure you're not calling an unitialized component_ID/handle, or verify that your weak_ptr is actually valid.");
			if (index.valid())
			{
				component_vec<component>* sub_storage = storage->get_storage<component>();
				sub_storage->get_component(index) = std::forward<component>(right);
			}
			return *this;
		}
		handle clone() const
		{
			assert(index.valid() && "Check to make sure you're not calling an unitialized component_ID/handle, or verify that your weak_ptr is actually valid.");
			if (!(index.valid() && component_id->get_value<component>() != component_ID::TYPE_NOT_INIT))
				return create_invalid_handle();
			return handle(storage->get_storage<component>()->clone_slot(index));
		}
		void set_owning_entity(unsigned int entity_id)
		{
			storage->get_storage<component>()->set_handles_owning_entity(index, entity_id);
		}
		//yes this is dumb, and it looks dumb to me at the time of writing, but its one of the only ways
		//to get it to work, and get it to work without duplicating code. (no need for a special constructor)

		//if we need to signal a failure to find a handle, use this as the signaler.
		static handle create_invalid_handle()
		{
			return handle_base();
		}
		private:
		//these are private, because vector is trying to use higher specialization and these somehow bind better than the other alternatives.
		
		//the storage for handles is a std::vector of components. everything is laid out in contiguous memory and does not support polymorphism.
		//to prevent unexpected object slicing there are operators to bind the derived types (templated below)
		//and then made private & deleted so they can't be accessed or used.
		//if you are absolutely sure you mean to slice the object, down cast it before attempting to assign. that'll still work fine.
		template<typename derived_component>
		handle(const derived_component&) = delete;
		template<typename derived_component>
		handle& operator=(const derived_component& right) = delete;
	};
	//these are never considered valid by operator bool/is_valid, due to never actually storing extra data elsewhere. 
	//it's just a handle, turned flag that works with the standard system parser.
	template<typename flag>
	class handle_flag : public handle_base
	{
		handle_flag()
		{
			index = INVALID_INDEX;
			comp_id = component_id->get_value<flag>();
		}
		handle_flag(const handle_flag&) = default;
		handle_flag(handle_flag&&) = default;
		handle_flag& operator=(const handle_flag&) = default;
		handle_flag& operator=(handle_flag&&) = default;
	};

	template<typename component>
	class weak_handle : disable_type<component, spawnx::handle_base>, disable_nesting<component, spawnx::handle, spawnx::weak_handle>
	{
		component_index index;
		unsigned int comp_id;
	public:
		weak_handle() = delete;
		weak_handle(const handle<component>& _handle)
		{
			index = _handle.index;
			comp_id = component_id->get_value<component>();
		}
		weak_handle(const weak_handle&) = default;

		handle<component> lock() const
		{
			return expired() ? handle<component>::create_invalid_handle() : handle<component>(index);
		}
		bool expired() const
		{
			if (index.valid())
			{
				component_vec_base* storage_vec = handle_base::storage->get_storage_unsafe(comp_id);
				return !storage_vec->component_unchanged(index);	//if the component has changed or is invalid, this returns false, so we need to negate it.
			}
			return true;
		}
	};
	//used to indicate we want all matching types. use template specialization to capture the actual template.
	template<typename type>
	struct all {};
	//used to indicate we want no matching types. use template specialization to capture the actual template.
	template<typename type>
	struct none {};

	//use as: typename handle_ret_solver<components>::type...
	//this is meant to help resolve various return types that an individual component would break down into.
	//e.g. a component Vector3 becomes handle<Vector3>.
	//all<Vector3> becomes std::vector<handle<Vector3>>.
	//none<Vector3> becomes.. std::tuple<> < kek? -- it's meant to be used with tuple_cat<> and will be auto removed from handle_pack via remove_t<tuple<>, ...>

	template<typename ty>
	struct handle_ret_solver
	{
		using type = handle<ty>;
	};
	template<typename ty>
	struct handle_ret_solver<all<ty>>
	{
		using type = std::vector<handle<ty>>;
	};
	template<typename ty>
	struct handle_ret_solver<none<ty>>
	{
		using type = std::tuple<>;
	};

	//this class exsits solely to make using a tuple easier.
	//this allows us to go..
	//handle_pack.get<component>();
	//instead of
	//get<typename handle_ret_solver<component>>(tuple);
	template<typename ... components>
	class handle_pack
	{
		remove_t<std::tuple<>, typename handle_ret_solver<components>::type...> handles;
	public:
		handle_pack() = delete;
		explicit handle_pack(remove_t<std::tuple<>, typename handle_ret_solver<components>::type...>&& _handles) : handles(_handles){}

		template<typename component>
		typename handle_ret_solver<component>::type& get()
		{
			return std::get<typename handle_ret_solver<component>::type>(handles);
		}
		template<size_t index>
		decltype(auto) get()
		{
			return std::get<index>(handles);
		}
	};
}