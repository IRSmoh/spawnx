#include <iostream>
#include "spawnx.h"
#include <string>
struct name
{
	std::string str;
	name(const std::string& _str) : str(_str) {}
	name(const char* _str) : str(_str) {}
	name() {}
	void Print()
	{
		std::cout << "entities name is: " << str << std::endl;
	}
	inline friend std::string get_string(name& _name)
	{
		return _name.str + '\n';
	}
};
struct Vector2
{
	float x, y;
	Vector2(float _x, float _y) : x(_x), y(_y) {}
	Vector2():x(0),y(0) {}
	inline friend std::string get_string(Vector2& vec)
	{
		return std::string("{") + std::to_string(vec.x) + std::string(",") + std::to_string(vec.y) + std::string("}\n");
	}
};
struct name_system : public spawnx::system<name>
{
	void update(double delta_time)
	{
		for (auto& pack : handle_packs)
		{
			pack.get<name>()->Print();
		}
	}
};
//showing that we can use multiple component types in a single system.
struct height_check_system : public spawnx::system<name, Vector2>
{
	void update(double delta_time)
	{
		for (decltype(auto) pack : handle_packs)
		{
			//this is done to prove you can dereference the handle and keep a valid reference to your data
			//thus preventing calling pack.get<>() and the handle deref types all the time.
			//they are lite... but they still have to do a couple effective pointer chases.
			Vector2& vec = *pack.get<Vector2>();
			if (vec.y <= 0.f)
			{
				pack.entity->add_component(std::string("Adding a handle during a system update\n"));
				std::cout << "The entity by name: [" << pack.get<name>()->str << "] is below y = 0.\n";
			}
		}
	}
};
//a simple system that emits an event.
struct height_event_emitter_system : public spawnx::system<Vector2>
{
	void update(double delta_time)
	{
		for (decltype(auto) pack : handle_packs)
		{
			Vector2& vec = *pack.get<Vector2>();
			if (vec.x < 0 && vec.y < 0)
			{
				//spawnx::system has a helper function called emit_event<> where we can feed it our data easily.
				//otherwise we'd have to remember the longer version.. event_manager->add_event(_event);
				emit_event(vec);
				emit_event(vec, 1);
			}
		}
	}
};

//a free standing function that we bind as a delegate to parse given events.
//the event in this case being a simple vector2.
//note: all functions that will be a delegate must be of the form func(const <type>& ...)
void process_vec2_event(const Vector2& vec)
{
	std::cout << "This vector2 is in quadrant 3. [x < 0 , y <0]\nAnd the vec is: [" << vec.x << "," << vec.y << "]\n";
}
int main()
{
	//create the world again.
	spawnx::world world;
	//initialize our component types, so we can have valid storage.
	world.component_id.init_value<name, Vector2>();
	//we also have to initialize the event type as events and components do not share storage (since they are different things).
	world.component_id.init_value<Vector2>();
	world.component_id.init_value<std::string>();
	//create our entity again.
	spawnx::weak_handle<spawnx::entity> entity = world.create_entity();
	if (spawnx::handle<spawnx::entity> e = entity.lock())
	{
		//add multiple components in one go.
		e->add_component<name, Vector2>("asd", Vector2(-10, -1));
	}

	auto entity2 = world.create_entity();
	if (auto e = entity2.lock())
	{
		e->add_component<Vector2>(Vector2(-1,-1));
	}
	auto entity3 = world.create_entity();
	if (auto e = entity3.lock())
	{
		e->add_component<Vector2>(Vector2(-2, -1));
	}
	if (auto e = entity2.lock())
	{
		e->add_component<Vector2>(Vector2(-3, -1));
	}
	if (auto e = entity2.lock())
	{
		e->add_component<Vector2>(Vector2(-4, -1));
	}

	//register our systems, this version indicates we can have non default constructed systems
	//but for this example we just default construct anyway.
	world.add_system(name_system());
	//register multiple default constructed systems.
	world.add_system<height_check_system, height_event_emitter_system>();

	//like component_ID's we need to reserve our event type. (this is so mods work well with things that for some insane reason we serialize the events.)
	spawnx::event_id->init_value<Vector2>();
	//also showing you can reserve a group of arguments being passed as an event.
	world.event_id.init_grouped_value<Vector2, int>();
	//register the previously done listener. Note: the underlying delegate is auto deduced from the parameters to the function.
	world.add_event_listener(&process_vec2_event);
	//even though it doesn't do anything with the event you can add a class as an event listener also.
	struct tmp : public spawnx::flag_wrapper
	{
		void func(const Vector2&) {}
	};
	tmp temp;
	world.add_event_listener(&temp, &tmp::func);
	//a lambda is simple.
	world.add_event_listener([](const Vector2&){});
	//variadic / "polymorphic" lambdas are a bit more difficult. in that you have to specify the overload you want to use. 
	//note: const ref qualifiers are not manadatory to specify (they're stripped)
	//but matching the calling convetion exactly (const <type>& by default) allows us to decay the lambda to a function pointer
	//enabling some further cache optimizations.
	world.add_event_listener<Vector2>([](auto vec)->void { std::cout << "Calling from auto lambda: {" << vec.x << "," << vec.y << "}\n"; });
	//update as always.
	world.sort_components();
	world.print_entities();
	world.update(0);

	//we can register entity templates also.
	//reserve the id's we're going to be working with first...
	world.component_id.init_value<int, float, double>();
	//register the template and its default values (first part of the field is the likelihood this entity is selected from all candidates).
	world.register_entity_template( 1, 10, 0.f, 1.0, Vector2(-111123,-456));
	//add entity templates to the world.
	world.create_template_entity<int,float>(1);	//since we registered an entity with handles of int/float/double we can find an entity that contains any combo of those.
	world.create_template_entity<int, std::string>(std::string("this can't happen create a new entity"));	//this on the other hand does not find an entity template and thus is ignored.
	world.update(0);
	world.print_entities();


	//just testing how events with multiple inputs would work.
	world.event_id.init_grouped_value<int, double, std::string>();
	auto t = world.event_id.get_grouped_value<int, double, std::string>();
	std::cout << world.event_id.get_name(t) << '\n';
}