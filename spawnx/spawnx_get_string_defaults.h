#pragma once
#include <string>
namespace spawnx
{
	inline std::string get_string(std::string& comp)
	{
		return comp;
	}
	template<typename type, std::enable_if_t<std::is_arithmetic<type>::value, bool> = true>
	inline std::string get_string(type& comp)
	{
		return std::to_string(comp) +'\n';
	}
}