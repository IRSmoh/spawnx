#pragma once
#include "component_ID.h"
namespace spawnx
{
	//the point of this class is 'simple'
	//we have to be able to store a vector of component vectors.
	//aka, we don't specifically know how many components we will have.
	//but we have to be able to access them by their type, probably best done through hash codes.
	//in any case, we =need= type safety to ensure sanity, which means we'll need some polymorphism
	//to deal with the base storage.
	//To get the right type we can compare with the requested type, determine its hash code, and then see if we have the type.
	//if that fails we can return null.
	//to convert the base class to a derived version dynamic_cast() probably will work best.

	struct component_index
	{
		unsigned int index;
		unsigned int id;
		component_index(){}
		component_index(unsigned int _index, unsigned int _id) : index(_index) , id(_id) {}
		bool operator==(component_index right) const
		{
			return index == right.index && id == right.id;
		}
		bool operator>(component_index right) const
		{
			return index > right.index;
		}
		bool operator<(component_index right) const
		{
			return index < right.index;
		}
		bool operator!=(component_index right) const
		{
			return !(*this == right);
		}
		bool operator>=(component_index right) const
		{
			return !(*this < right);
		}
		bool operator<=(component_index right) const
		{
			return !(*this > right);
		}
		bool valid() const
		{
			return index != -1 && id != -1;
		}
	};
	class component_vec_base
	{
		template<typename component> friend class handle;
		friend class handle_base;

		template<typename component> friend class weak_handle;

		friend class world;
		friend class component_storage;
	protected:

		//makes it slightly clearer what we're doing here.
		struct open_offset_slot
		{
			component_index slot;
			open_offset_slot(component_index _slot) : slot(_slot)
			{}
		};
		struct handle_offset
		{
			unsigned int offset;
			handle_offset(unsigned int _offset) : offset(_offset) {}
			bool operator ==(const handle_offset& other) const
			{
				return offset == other.offset;
			}
			bool operator!=(const handle_offset& other) const
			{
				return !(*this == other);
			}
		};

		const static handle_offset EMPTY_SLOT;
		std::vector<handle_offset> handle_offsets;
		std::vector<open_offset_slot> open_slots;
		unsigned int comp_id;
		const static component_index INVALID_INDEX;
	public:
		component_vec_base()
		{}
		virtual ~component_vec_base() {}
	private:
		virtual void increment_active_component(component_index handle_index) {}
		virtual void decrement_active_component(component_index handle_index) {}
		virtual component_index clone_slot(component_index handle_index) { return INVALID_INDEX; }
		virtual bool component_unchanged(component_index handle_index) const { return false; }
		virtual void set_handles_owning_entity(component_index handle_index, unsigned int entity_id) {}
		virtual void set_handles_owning_entity(component_index handle_index, component_index other_handle_index) {}
		virtual unsigned int get_owning_entity(component_index handle_index) { return -1; }
		virtual std::string get_string_of_component(component_index handle_index) { return ""; }
		virtual void sort() {}
	};

	template<typename component>
	class component_vec : public component_vec_base
	{
		template<typename all>
		friend class handle;
		friend class handle_base;

		template<typename all> friend class weak_handle;

		friend class world;
	protected:

		struct component_stat
		{
			component_index our_handle_index;
			int ref_counter;
			unsigned int owning_entity_id;
			component_stat(component_index _index) : our_handle_index(_index), ref_counter(0), owning_entity_id(-1)
			{}
			component_stat(component_index _index, unsigned int _owning_entity_id) : our_handle_index(_index),ref_counter(0),owning_entity_id(_owning_entity_id)
			{}
			bool operator<(const component_stat& right)
			{
				return owning_entity_id < right.owning_entity_id;
			}
		};

		//we use it for ref counting to determine when to destruct a component
		//and we use it to assist in swapping components/updating their handle's indirections when the component is destroyed.

		std::vector<component_stat> component_stats;

		std::vector<component> components;
		unsigned int component_id_counter;
	public:

		component_vec() : component_vec_base()
		{
			comp_id = component_id->get_value<component>();
			component_id_counter = 0;
		}
		~component_vec() {};
	private:
		component& get_component(component_index handle_index)
		{
			handle_offset slot = handle_offsets[handle_index.index];
			return components[slot.offset];
		}
		void destroy_component(component_index handle_index)
		{
			handle_offset slot = handle_offsets[handle_index.index];
			//swap our slot in components for the end.
			components[slot.offset] = std::move(components[components.size() - 1]);
			//also swap our stats that are tied to the component
			component_stats[slot.offset] = std::move(component_stats[component_stats.size() - 1]);
			//update the now swapped component offset in the handle offsets.

			handle_offsets[component_stats[slot.offset].our_handle_index.index] = slot;

			//I'm leaving this commented out as a reminder to not do this.
			//normally that seems stupid, but apparently it's an easy mistake to make, as I already made it once and it seemed correct.
			//this will cause components to clash in their believed locations.
			//component_stats[slot].our_handle_index = slot;

			//pop_back() our component to destruct it. (find some proper way of signaling to the handle it's invalid?, or just assume it's invalid?)
			components.pop_back();
			component_stats.pop_back();

			//check to see if we are the last offset in handle_offsets, if not signal that we can be reused
			if (handle_index.index >= handle_offsets.size() - 1)
			{
				handle_offsets.pop_back();
			}
			else
			{
				//if we are to be reused, place us into the open_slots. 
				handle_offsets[handle_index.index] = EMPTY_SLOT;
				open_slots.push_back(handle_index);
			}
		}
		component_index assign_new_slot()
		{
			return assign_new_slot(component());
		}
		//yes this is essentially a duplicated version of the above.
		//this is so we can have the component be passed in the constructor of the handle.
		component_index assign_new_slot(component&& comp)
		{
			//get the handle's index that we'll be returning.
			component_index handle_index = INVALID_INDEX;
			unsigned int handle_id = component_id_counter++;
			
			//all handles are proxied through a single vector to learn their datas location.
			//when we delete a handle we leave a hole in the afformentioned indirection vector.
			//so we should take an available slot from that first.
			if (open_slots.size())
			{
				handle_index = open_slots[open_slots.size() - 1].slot;
				handle_index.id = handle_id;
				open_slots.pop_back();
			}

			if (handle_index.index < handle_offsets.size())
			{
				//we have been given a valid handle index; we can use this and do not have to create a new one.
				handle_offsets[handle_index.index] = handle_offset((unsigned int)components.size());
			}
			else
			{
				//there were no open indexes for us to take, so we have to generate a new one.
				//note, the only way for us to have to create a new index in the first place is if the handle_offsets is fully reservered.
				//and conveniently handle_offsets size is always the same as components.size() (one to one relation between em all).
				handle_index = component_index((unsigned int)components.size(), handle_id);
				handle_offsets.push_back(handle_index.index);
			}

			//construct our new component
			components.push_back(std::forward<component>(comp));
			component_stats.push_back(component_stat(handle_index));

			//return our handle, now that everything else is done and it's good to use.
			return handle_index;
		}
		void increment_active_component(component_index handle_index) override final
		{
			if (!handle_index.valid())
				return;
			const handle_offset& slot = handle_offsets[handle_index.index];
			component_stats[slot.offset].ref_counter++;
		}
		void decrement_active_component(component_index handle_index) override final
		{
			if (!handle_index.valid())
				return;
			const handle_offset& slot = handle_offsets[handle_index.index];
			component_stats[slot.offset].ref_counter--;

			if (component_stats[slot.offset].ref_counter <= 0)
			{
				destroy_component(handle_index);
			}
		}
		component_index clone_slot(component_index handle_index) override final
		{
			if (!handle_index.valid())
				return INVALID_INDEX;
			return assign_new_slot(std::forward<component>(get_component(handle_index)));
		}
		//use this to verify that your component stats (id/index have not changed.)
		//the only real way for them to change would be the component being deleted (probable)
		//or the component slot being reused.
		//if the slot is reused, the id will differ. (unless you've managed to hold onto it for uint.max time..)
		bool component_unchanged(component_index handle_index) const override final
		{
			if (handle_index.index >= handle_offsets.size())
				return false;	//using an out of bounds handle.
			const handle_offset& slot = handle_offsets[handle_index.index];
			if (slot.offset >= component_stats.size())
				return false;	//using an out of bounds slot (not sure how this would be hit.. but doesn't hurt to check.
			return component_stats[slot.offset].our_handle_index == handle_index;
		}
		void set_handles_owning_entity(component_index handle_index, unsigned int entity_id) override final
		{
			//find our component_stats; change the field for entity_id to our supplied one.
			//we need to do this any time a handle/component data is given a new owner so we can sort things properly.
			const handle_offset& slot = handle_offsets[handle_index.index];
			component_stats[slot.offset].owning_entity_id = entity_id;
		}
		void set_handles_owning_entity(component_index handle_index, component_index other_handle_index) override final
		{
			set_handles_owning_entity(handle_index, get_owning_entity(other_handle_index));
		}
		unsigned int get_owning_entity(component_index handle_index) override final
		{
			const handle_offset& slot = handle_offsets[handle_index.index];
			return component_stats[slot.offset].owning_entity_id;
		}
		std::string get_string_of_component(component_index handle_index) override final
		{
			return get_string(get_component(handle_index));
		}
		void sort() override final
		{
			std::sort(zip::random(component_stats.begin(), components.begin()), zip::random(component_stats.end(), components.end()), [](auto&& left, auto&& right)
			{
				return std::get<0>(left) < std::get<0>(right);
			});
			//now that we've sorted the components/component stats we have to go back over them to re-couple the handles to the relocated data.
			//the easiest way I can currently think of is to iterate (by index, not auto&:container) over component_stats; use the index of our stats as the new location
			//and then reassign the handle_offsets given by component_stats to our working index.

			for (unsigned int ind(0), size((unsigned int)component_stats.size()); ind < size; ++ind)
			{
				handle_offsets[component_stats[ind].our_handle_index.index] = ind;
			}
		}
	};


	class component_storage
	{
		template<typename component> friend class handle;
		friend class handle_base;

		template<typename component> friend class weak_handle;

		friend class world;
		//we need a vector of polymorphic types.
		//I don't want to worry about destruction of the type, so we could use shared/unique pointers.
		//nothing else should -ever- own this, but it should be editable by others.
		//unique_ptr<> sounds best so far, so that's whats going to be used.

		std::vector<std::unique_ptr<component_vec_base>> storage;
		component_storage() {};
		//we own a vector of unique_ptrs, we need to specify the class is move only. else if we don't and try to copy construct visual studio barfs with unuseful error messages.

		component_storage(const component_storage&) = delete;
		component_storage(component_storage&&) = default;
		component_storage& operator=(const component_storage&) = delete;
		component_storage& operator=(component_storage&&) = default;

		~component_storage() {};

		template<typename T>
		component_vec<T>* get_storage()
		{
			unsigned int index = component_id->get_value<T>();
			if (index == component_ID::TYPE_NOT_INIT)
				return nullptr;
			if (index >= storage.size() || storage[index] == nullptr)
			{
				add_storage<T>();
			}

			//assuming we are loading in data from the disc and ignoring init order that is.
			return dynamic_cast<component_vec<T>*>(storage[index].get());
		}
		component_vec_base* get_storage_unsafe(unsigned int comp_id) const
		{
			if (comp_id >= storage.size())
				return nullptr;
			return storage[comp_id].get();
		};
		//error codes:
		//0: success
		//1: type not init -- call component_ID::init_value<component_type>() to fix.
		//-1: no storage added, storage already exists (soft success)
		template<typename T>
		errors::error_code add_storage()
		{
			unsigned int index = component_id->get_value<T>();
			if (index == component_ID::TYPE_NOT_INIT)
				return errors::uninit_type;	//can't use this ID yet due to the ID not being init.
			if (index < storage.size())
			{
				if (storage[index])
				{
					return errors::duplicate_alloc;	//we've allocated storage for something in this slot
				}
				storage[index] = std::make_unique<component_vec<T>>(component_vec<T>());
				return errors::success;	//we've allocated storage for something beyond this slot, but this slot is empty (this uniq_ptr is a nullptr)
			}
			if (index > storage.size())
			{
				//we've got a request to make a 'high' index store.
				//aka we've somehow skipped the normal sequential pattern.
				for (size_t i = storage.size(); i < index; i++)
					storage.push_back(nullptr);
			}
			storage.push_back(std::make_unique<component_vec<T>>(component_vec<T>()));
			return errors::success;	// the standard case, we're just sequentially adding storage.
		}
		void sort_components()
		{
			for (auto& comps : storage)
			{
				if(comps)
					comps->sort();
			}
		}
	};
}