#pragma once
namespace spawnx
{
	//so we can store all systems in an array for easy updating.
	class base_system
	{
		friend class world;
	protected:
		static event_manager* event_manager;
		base_system() {}
	public:
		virtual ~base_system() {}
	private:
		virtual void update(double delta_time){}
		virtual void fetch_entities(std::vector<entity>& entities) {}
	};
	template<typename ... components>
	struct entity_and_handles
	{
		entity* entity;
		handle_pack<components...> handle_pack;
		template<size_t ind>
		decltype(auto) get()
		{
			return handle_pack.template get<ind>();
		}
		template<typename type>
		decltype(auto) get()
		{
			return handle_pack.template get<type>();
		}
	};
	template<typename ... components>
	class system : public base_system
	{
		friend class world;
	protected:
		using vec_pack = std::vector<entity_and_handles<components...>>;
		vec_pack handle_packs;
	public:
		using entity_and_handles = entity_and_handles<components...>;
		system() : base_system() {}
	private:
		void fetch_entities(std::vector<entity>& entities) override
		{
			vec_pack handle_packs;
			for (auto& entity : entities)
			{
				if (entity.has_component<components...>())
				{
					handle_packs.push_back({ &entity, entity.get_components<components...>() });
				}
			}
			this->handle_packs = std::move(handle_packs);	//we might be moving a ton of things. no sense wasting copy operations on something that can be faster.
		}
		void update(double delta_time) override
		{
			for (entity_and_handles& pack : handle_packs)
			{
				update(delta_time, pack);
			}
		}
		virtual void update(const double& delta_time, entity_and_handles& pack){}
	protected:
		template<typename... event_types>
		void emit_event(event_types&&... _event)
		{
			event_manager->add_event(std::forward<event_types>(_event)...);
		}
	};
}