#pragma once
namespace spawnx
{
	class world
	{
		event_manager events_manager;
		component_storage comp_storage;
		std::vector<handle<entity>> entities;
		std::vector<std::unique_ptr<base_system>> systems;
		std::vector<std::pair<entity, size_t>> template_entities;
		std::mt19937 rand_gen;
	public:
		component_ID component_id;
		event_ID event_id;
		world() : rand_gen(std::random_device()())
		{
			sync_statics();
		}
		~world() {}

		void sync_statics()
		{
			component_id.init_value<entity>();
			spawnx::component_id = &component_id;
			spawnx::event_id = &event_id;
			base_system::event_manager = &events_manager;
			handle_base::storage = &comp_storage;
		}
		weak_handle<entity> create_entity()
		{
			entities.push_back(entity());
			return entities.back();
		}
		weak_handle<entity> clone_entity(handle<entity>& e)
		{
			entities.push_back(e->clone());
			return entities.back();
		}
		//verify that the passed handle is valid; if it is valid clone it and store it.
		//if the handle is invalid just return an invalid weak_handle.
		weak_handle<entity> clone_entity(weak_handle<entity>& e)
		{
			if (auto handle = e.lock())
			{
				entities.push_back(handle->clone());
				return entities.back();
			}
			return handle<entity>::create_invalid_handle();
		}
		template<typename ... entity_handle_types>
		entity register_entity_template(size_t entity_weight, entity_handle_types&&... components)
		{
			entity ent;
			ent.add_component(std::forward<entity_handle_types>(components)...);
			template_entities.push_back({ std::move(ent),entity_weight });
			return template_entities.back().first;
		}
		
		template<typename ... entity_handle_types, typename... replacement_handle_values>
		weak_handle<entity> create_template_entity(replacement_handle_values&&... new_handle_values)
		{
			entity ent = find_template_entity<entity_handle_types...>();
			if (!ent.handles.empty())
			{
				entity e = ent.clone();
				const auto lam = [](entity& e, auto&& data)
				{
					using data_type = typename std::remove_reference<decltype(data)>::type;
					e.get_component<data_type>() = std::forward<data_type>(data);
				};
#if defined(_MSC_VER) && !defined(__clang__)
				//when visual studio finally supports fold expressions remove this chunk and just use the below version.
				[](auto&&...) {}( (lam(e, std::forward<replacement_handle_values>(new_handle_values)), 0)... );
#else
				//fuck yeah fold expressions
				(lam(e, std::forward<replacement_handle_values>(new_handle_values)), ...);
#endif
				entities.push_back(std::move(e));
				return entities.back();
			}
			return handle<entity>::create_invalid_handle();
		}
		template<typename system>
		void add_system()
		{
			systems.push_back(std::make_unique<system>(system()));
		}
		template<typename system>
		void add_system(system&& sys)
		{
			systems.push_back(std::make_unique<system>(std::forward<system>(sys)));
		}
		template<typename system, typename system_2, typename...rest>
		void add_system()
		{
			add_system<system>();
			add_system<system_2, rest...>();
		}
		template<typename system, typename system_2, typename...rest>
		void add_system(system&& sys, system_2&& sys_2, rest&&... rem)
		{
			add_system(std::forward<system>(sys));
			add_system(std::forward<system_2>(sys_2), std::forward<rest>(rem)...);
		}
		template<typename callable>
		void add_event_listener(callable _callable)
		{
			events_manager.add(_callable);
		}
		template<typename first_arg, typename ... rest_args, typename templated_lambda>
		void add_event_listener(templated_lambda lambda)
		{
			events_manager.add<first_arg, rest_args...>(lambda);
		}
		template<typename obj, typename obj_mem_func>
		void add_event_listener(obj* object, obj_mem_func func)
		{
			events_manager.add<obj, obj_mem_func>(object, func);
		}

		//literally all the same as above just with the library user specifying when the objects destruct vs auto managed/permanent.

		template<typename callable>
		del_destruct_flag add_event_listener_self_managed(callable _callable)
		{
			return events_manager.add(_callable);
		}
		template<typename first_arg, typename ... rest_args, typename templated_lambda>
		del_destruct_flag add_event_listener_self_managed(templated_lambda lambda)
		{
			return events_manager.add<first_arg, rest_args..., templated_lambda>(lambda);
		}
		template<typename obj, typename obj_mem_func>
		del_destruct_flag add_event_listener_self_managed(obj* object, obj_mem_func func)
		{
			return events_manager.add<obj, obj_mem_func>(object, func);
		}
		void update(double delta_time)
		{
			remove_destroyed_entities();
			update_systems(delta_time);
			update_events(delta_time);
		}
		void sort_components()
		{
			comp_storage.sort_components();
		}
		void print_entities()
		{
			for (auto& entity : entities)
			{
				std::cout << entity->get_string();
			}
		}
	private:
		void update_systems(double delta_time)
		{
			component_vec<entity>* stored_entities = comp_storage.get_storage<entity>();
			for (decltype(auto) system : systems)
			{
				system->fetch_entities(stored_entities->components);
				system->update(delta_time);
			}
		}
		void update_events(double delta_time)
		{
			events_manager.update();
		}
		void remove_destroyed_entities()
		{
			auto dest_lam = [](handle<entity>& e)
			{
				return (*e).destroy_entity;
			};
			entities.erase(std::remove_if(entities.begin(), entities.end(), dest_lam), entities.end());
		}

		template<typename ... entity_handle_types>
		entity find_template_entity()
		{
			//not sure if this should be a private method or not..
			//the entities that are templated can be incorrectly edited by using this function very, very easily.
			//the entity handles will vastly outstrip the life of this function (they do not get destroyed until engine is closed).
			std::vector<std::pair<entity, size_t>> candidate_entities;
			size_t cummulative_weight = 0;
			for (const auto& entity_and_weight : template_entities)
			{
				if (entity_and_weight.first.has_component<entity_handle_types...>())
				{
					candidate_entities.push_back(entity_and_weight);
					cummulative_weight += entity_and_weight.second;
				}
			}
			//make sure we dont have the trivial case of only 1 match.
			if (candidate_entities.size() == 1)
				return candidate_entities[0].first;

			//alright we dont. we can move onto more reasonable things.
			//yeah we could probably get a faster result with just mod. but its not properly weighted..
			std::uniform_int_distribution<size_t> distribution(0, cummulative_weight);
			size_t rand_num = distribution(rand_gen);
			for (const auto& entity_and_weight : candidate_entities)
			{
				//check to see if we can skip over this entity template, if not then we've 'drawn' it.
				if (rand_num <= entity_and_weight.second)
					return entity_and_weight.first;
				else
					rand_num -= entity_and_weight.second;	//we can just use this as a counter...
			}
			return entity();
		}
	};
}