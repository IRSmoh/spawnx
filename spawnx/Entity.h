#pragma once
namespace spawnx
{
	class entity
	{
		template<typename ... all>
		friend class system;
		friend class world;

		std::vector<handle_base> handles;
		unsigned int entity_id;
		static unsigned int id_counter;
		bool destroy_entity;

		template<typename component>
		struct impl_has
		{
			static bool has_component(const std::vector<handle_base>& handles)
			{
				unsigned int comp_id = component_id->get_value<component>();
				if (comp_id == component_ID::TYPE_NOT_INIT)
					return false;
				for (decltype(auto) handle : handles)
				{
					if (handle.comp_id == comp_id)
					{
						return true;
					}
				}
				return false;
			}
		};
		template<typename component>
		struct impl : public impl_has<component>
		{
			static handle<component> get_component(const std::vector<handle_base>& handles)
			{
				unsigned int comp_id = component_id->get_value<component>();
				for (decltype(auto) curr_handle : handles)
				{
					if (curr_handle.comp_id == comp_id)
					{
						return (handle<component>)curr_handle;
					}
				}
				return handle<component>::create_invalid_handle();
			}
		};
		template<typename component>
		struct impl<all<component>> : public impl_has<component>
		{
			static std::vector<handle<component>> get_component(const std::vector<handle_base>& handles)
			{
				unsigned int comp_id = component_id->get_value<component>();
				std::vector<handle<component>> components;
				for (decltype(auto) curr_handle : handles)
				{
					if (curr_handle.comp_id == comp_id)
					{
						components.push_back((handle<component>)curr_handle);
					}
				}
				return components;
			}
		};
		template<typename component>
		struct impl<none<component>> : public impl_has<component>
		{
			static typename handle_ret_solver<none<component>>::type get_component(const std::vector<handle_base>& handles)
			{
				return typename handle_ret_solver<none<component>>::type();
			}
			//returns negated compared to normal (so that a system will select entities properly.)
			//e.g. if we have a component (and we're requesting we don't) we'll return false.
			//if we don't have a component (and we don't want it to.) we'll return true.
			static bool has_component(const std::vector<handle_base>& handles)
			{
				return !impl_has<component>::has_component(handles);
			}
		};
		template<typename ... components>
		struct impl<std::tuple<components...>>
		{
			static std::tuple<typename handle_ret_solver<components>::type...> get_components(const std::vector<handle_base>& handles)
			{
				return std::tuple<typename handle_ret_solver<components>::type...>(impl<components>::get_component(handles)...);
			}
		};
		//call has_component<components...>() before calling this. else you will receive garbage.
		template<typename ... components>
		handle_pack<components...> get_components() const
		{
			//by using remove_specialization we can ignore creating temp objects and just directly filtering by the components we do actually wish to pass back.
			//e.g. by calling remove_specilization<none,...> we ignore creating a tuple with any none<> struct; and create it with the remaining components.
			return handle_pack<components...>(impl<remove_specialization<none, components...>>::get_components(handles)); 
		}
	public:
		entity()
		{
			entity_id = id_counter++;
			destroy_entity = false;
		}
		~entity() {}

		const unsigned int& our_id() const
		{
			return entity_id;
		}
		template<typename component>
		typename handle_ret_solver<component>::type get_component() const
		{
			return impl<component>::get_component(handles);
		}
		template<typename component>
		void add_component()
		{
			handle<component> handle(entity_id);
			if (handle)
			{
				handles.push_back(handle);
			}
		}
		template<typename component, typename component_2, typename ... rest>
		void add_component()
		{
			add_component<component>();
			add_component<component_2, rest...>();
		}
		template<typename component>
		void add_component(component&& comp)
		{
			handle<component> handle(std::forward<component>(comp),entity_id);
			if (handle)
			{
				handles.push_back(handle);
			}
		}
		template<typename component, typename component_2, typename ... rest>
		void add_component(component&& comp, component_2&& comp_2, rest&&... remaining)
		{
			add_component<component>(std::forward<component>(comp));
			add_component<component_2, rest...>(std::forward<component_2>(comp_2), std::forward<rest>(remaining)...);
		}
		template<typename component>
		void remove_component()
		{
			unsigned int comp_id = component_id->get_value<component>();
			//no sense checking for a component we cannot have.
			if (comp_id == component_ID::TYPE_NOT_INIT)
				return;
			for (size_t ind = handles.size()-1; ind !=-1; ind--)
			{
				if (handles[ind].comp_id == comp_id)
				{
					handles.erase(handles.begin() + ind);
				}
			}
		}
		template<typename component_1, typename component_2, typename ... rest>
		void remove_component()
		{
			remove_component<component_1>();
			remove_component<component_2, rest...>();
		}
		template<typename component>
		bool has_component() const
		{
			return impl<component>::has_component(handles);
		}
		template<typename component, typename component_2, typename ... rest>
		bool has_component() const
		{
			return has_component<component>() && has_component<component_2, rest...>();
		}
		entity clone() const
		{
			entity new_entity;
			for (decltype(auto) component : handles)
			{
				new_entity.handles.push_back(component.clone());
			}
			return new_entity;
		}
		void mark_for_deletion()
		{
			destroy_entity = true;
		}
		std::string get_string()
		{
			auto tmp = std::string("Entity: ") + std::to_string(entity_id) + "\n";
			for (auto& handle : handles)
			{
				tmp += handle.get_string();
			}
			return tmp;
		}
	};

}