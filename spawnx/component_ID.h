#pragma once
#include "component_id_loader.h"
#include <numeric>
//a simple way of getting a unique ID for each type.
//note that call order does matter for this, so standardize on a first call location.
namespace spawnx
{
	template<typename category>
	class ID_assigner
	{
		friend struct event_id_assigner;
		component_id_loader id_loader{};

		template<typename... components>
		unsigned int value(bool gen_id)
		{
			unsigned int candidate_id = 0;
			static bool gend = false;
			if (!gend && !gen_id)
				return TYPE_NOT_INIT;
			if (!gend)
			{
				std::string id_name;
#ifndef FOLD_EXPRESSIONS

				std::initializer_list<std::string> tmp_until_fold_expressions{((std::string)type_name<components>() + '|')... };
				id_name = std::accumulate(tmp_until_fold_expressions.begin(), tmp_until_fold_expressions.end(), std::string(""));
#else
				id_name = ((std::string)type_name<components>() + '|' + ...);
#endif
#ifdef _MSC_VER
				//visual studio gives us a type name format of... "struct type<struct sub_type>" or "class type<class sub_type>" while gcc/clang gives us just "type<sub_type>"
				//so theres no reason to do more work on the other compilers...

				string_remove_all(id_name, std::string("struct "));
				string_remove_all(id_name, std::string("class "));
#endif
				//apparently the compilers can't agree on how things should be spaced out... so just strip the spaces.
				string_remove_all(id_name, std::string(" "));
				candidate_id = (uint32_t)id_loader.get_id(id_name);
			}
			gend = true;
			const static unsigned int id = candidate_id;
			return id;
		}
	public:
		const static unsigned int TYPE_NOT_INIT = -1;
		template<typename component>
		unsigned int init_value()
		{
			return value<component>(true);
		}
		template<typename component, typename component_2, typename ... rest>
		void init_value()
		{
			init_value<component>();
			init_value<component_2, rest...>();
		}
		template<typename component>
		unsigned int get_value()
		{
			return value<component>(false);
		}
		decltype(auto) get_name(unsigned int ID) const
		{
			return id_loader.get_name_from_ID(ID);
		}
	};
	struct event_id_assigner : ID_assigner<struct event>
	{
		template<typename component, typename ... rest>
		void init_grouped_value()
		{
			value<component, rest...>(true);
		}
		template<typename component, typename... rest>
		unsigned int get_grouped_value()
		{
			return value<component, rest...>(false);
		}
	};
	using component_ID = ID_assigner<struct component>;
	using event_ID = event_id_assigner;

	//reallyyyyyyyyyyyyyy aught to find a better way of giving everything easy access to these.
	//NOTE: these are owned by "world" these are just convience of access pointers (and yes.. less encapsulation. sue me.)

	extern component_ID* component_id;
	extern event_ID* event_id;
}