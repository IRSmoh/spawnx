#pragma once
namespace spawnx
{
	class component_id_loader
	{
		struct id_and_index
		{
			std::string id_name;
			size_t actual_index;
		};
		std::vector<id_and_index> load_these_ids;
		std::vector<std::string> current_ids;
		const std::string invalid_id_string = "ID not defined.";
		size_t find_id(const std::string& name)
		{
			size_t ind{ 0 }, max{ load_these_ids.size() };
			for (; ind < max; ind++)
			{
				if (load_these_ids[ind].id_name == name)
				{
					size_t id = load_these_ids[ind].actual_index;
					load_these_ids.erase(load_these_ids.begin() + ind, load_these_ids.begin() + ind + 1);
					return id;
				}
			}
			return -1;	//we could not find a name; use -1 as a signal.
		}
		size_t lookup_name(const std::string& name)
		{
			size_t id = find_id(name);
			if (id != -1)	//we found a name now we have to make it easy to store the data to disk again.
			{
				if (id >= current_ids.size())
				{
					for (size_t curr = current_ids.size(); curr <= id; curr++)
						current_ids.push_back("");
				}
				current_ids[id] = name;
			}
			return id;
		}
		//adds the desired name to our internal storage, the returned size_t is the valid component_ID for use.
		size_t add_name(const std::string& name)
		{
			size_t last_loaded_id = load_these_ids.empty() ? -1 : load_these_ids.back().actual_index;
			if (last_loaded_id != -1 && last_loaded_id >= current_ids.size())
			{
				for (size_t ind = current_ids.size(); ind <= last_loaded_id; ind++)
				{
					current_ids.push_back("");
				}
			}
			current_ids.push_back(name);
			return current_ids.size() - 1;
		}
	public:
		//actually deal with what happens when we are trying to put a new component in when another component is going to come along and expect that slot we stole.
		//so..... the way this is going to be set up. the only way the above condition can occur is if we've stored the level/state and have to reload it.
		//aka on loading a save or something w/ new mods.
		//meaning we know how many id's we're going to have. and where they belong.
		//which also should mean if we keep that initial bit sorted we can check to see if the vector's got more or less data than we expect.
		//meaning.....
		//curr_names{1,2,3,4,.,.,.,8} or something
		//loading_names{5,6,7,10} or w/e ignore the skipped number its to illustrate a point.
		//thus we can check the last loading_names data to see if its index is higher or lower than our current data (since mods could technically load out of order).
		//then apon confirming	it is higher	-> reserve space until we can add our data
		//							if not		-> just push_back our name and call it a day.
		//which then means we gotta erase in place our std::strings... but given they're just a begin/end pointer things should be stupidly easy to copy over.

		//if size_t is not -1 then name is a valid index for componet_ID use. the name is also then removed from the candidates to load.
		size_t get_id(const std::string& name)
		{
			for (size_t ind = 0; ind < current_ids.size(); ind++)
			{
				if (current_ids[ind] == name)
					return ind;
			}
			size_t tmp = lookup_name(name);
			if (tmp == -1)	//we could not load the name
				return add_name(name);
			return tmp;
		}
		decltype(auto) get_name_from_ID(size_t ID) const
		{
			return !current_ids.empty() && (ID < current_ids.size()) ? current_ids[ID] : invalid_id_string;
		}
		//visual studio is being a bit of a bitch about fopen and trying to sell us on fopen_s. great... but its non standard so no thanks.
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning( disable : 4996)
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#endif
		void load_component_ID_strings(const std::string& filename)
		{
			FILE* file = fopen(filename.c_str(), "rb");
			if (!file)
				return;

			fseek(file, 0, SEEK_END);
			size_t file_length = ftell(file);
			fseek(file, 0, SEEK_SET);

			std::vector<char> buffer(file_length + 1);
			buffer[file_length] = '\0';

			fread(buffer.data(), sizeof(char), file_length, file);
			fclose(file);

			size_t ind{ 0 };
			while (ind < file_length)
			{
				load_these_ids.push_back({ std::string(&buffer[ind]),load_these_ids.size() });
				ind += load_these_ids.back().id_name.size() + 1;
				//std::string tells us the size of chars we have minus the leading zero..
				//we've got a lot of leading zeros in this case so we need to move 1 past that to read all the data.
			}
		}
		void store_component_ID_strings(const std::string& filename)
		{
			FILE* file = fopen(filename.c_str(), "wb");
			if (!file)
				return;
			for (const auto& str : current_ids)
			{
				//std::string is null terminated (by standard)
				//thus we can just copy the null terminator over.. hence our +1.
				fwrite(str.data(), sizeof(char), str.size() + 1, file);
			}
			fclose(file);
		}
#ifdef _MSC_VER
#pragma warning(pop)
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
	};
}