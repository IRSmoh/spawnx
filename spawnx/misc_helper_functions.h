#pragma once
#include "static_string.h"
#include <string>

namespace spawnx
{
	template <class T>
	constexpr static_string type_name()
	{
#ifdef __clang__
		static_string str = __PRETTY_FUNCTION__;
		//the +5 is due to the format of __PRETTY_FUNCTION__ aka the left most '[' is -5 from the actual type name that we want.
		//e.g..... static_string type_name() [T = spawnx::demo]
		return static_string(str, find(str, '[') + 5, rfind(str, ']'));
#elif defined(__GNUC__)
		static_string str = __PRETTY_FUNCTION__;
		return static_string(str, find(str, '[') + 5, rfind(str, ']'));
#elif defined(_MSC_VER)
		static_string str = __FUNCSIG__;
		return static_string(str, find(str, '<') + 1, rfind(str, '>'));
#endif
	}
	static void string_remove_all(std::string& str, const std::string& match)
	{
		size_t start_offset{}, end_offset{}, matches{}, current_memmove_offset{}, move_size{};
		const size_t match_size{ match.size() }, str_size{ str.size() };

		//find the first instance of the matching string.
		//after finding the string note the start position.
		start_offset = str.find(match);
		current_memmove_offset = start_offset;
		while (start_offset < str_size)
		{
			//skip past insta matching itself again...
			//find until the next matching instance
			end_offset = str.find(match, start_offset + match_size);
			//move all the intermediate characters down, then proceed after the last match.

			if (end_offset != str.npos)
			{
				move_size = end_offset - start_offset - match_size;
				memmove(&str[current_memmove_offset], &str[start_offset + match_size], move_size);
			}
			else
			{
				move_size = str_size - start_offset - match_size;
				memmove(&str[current_memmove_offset], &str[start_offset + match_size], move_size);
			}

			//repeat until string is empty
			//note the number of matches
			matches++;
			current_memmove_offset += move_size;
			start_offset = end_offset;
		}
		//resize the string to the appropriate size
		//tada, a n^2 string replacement........ not like it can be much faster
		//gotta do a linear search to find the match anyway
		//then gotta copy the data over; best is to make it not copy the remaining strings a fuck load.
		str.resize(str_size - matches*match_size);
	}
}