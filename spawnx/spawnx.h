#pragma once
//stl
#include <vector>
#include <memory>
#include <cassert>
#include <tuple>
#include <string>
#include <cstdio>
#include <iostream>
#include <random>

//templates to check for c++17 feature support (horribly done but w/e)
#ifdef __clang__
#define FOLD_EXPRESSIONS
#endif
//meta things e.g. trying to wrangle template type deduction
#include "meta_templates.h"

//others
#include "delegate.h"
#include "zip_iterator.h"
#include "static_string.h"
#include "misc_helper_functions.h"
//constants
namespace spawnx
{
	//errors
	namespace errors
	{
		using error_code = unsigned int;
		constexpr error_code success = 0;
		constexpr error_code duplicate_alloc = -1;
		constexpr error_code uninit_type = 1;
	}
	//default get_string so we can do ADL lookup and get the right implementation for component::get_string in component_storage.h
	//that way we can print our components data without actually knowing the component type that entities own.
	template<typename component, std::enable_if_t<!std::is_arithmetic<component>::value, bool> = true>
	std::string get_string(component& comp)
	{
		return "Component does not supply get_string\n";
	}
}
#include "spawnx_get_string_defaults.h"
//specific to just spawnx namespace.
#include "component_storage.h"
#include "component_id_loader.h"
#include "component_ID.h"
#include "handle.h"
#include "entity.h"
#include "event.h"
#include "delegate.h"
#include "system.h"
#include "world.h"